#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <iostream>

namespace py = pybind11;

class NumpyClass {
public:
  NumpyClass(const py::array_t<double> Input_array) : Input_array(Input_array) { }
  void setArray(const py::array_t<double> arr);
  const py::array_t<double> getArray() const { return Input_array; }
private:
  py::array_t<double> Input_array;
};

void NumpyClass::setArray(const py::array_t<double> arr)
{
  py::buffer_info buf1 = arr.request();
  py::buffer_info buf2 = Input_array.request();

  if (buf1.ndim != 1 || buf2.ndim != 1)
    throw std::runtime_error("Number of dimensions must be one");

  if (buf1.size != buf2.size)
    throw std::runtime_error("Input shapes must match");
  Input_array = arr;
}



PYBIND11_MODULE(numpy_class, m) {
    py::class_<NumpyClass>(m, "NumpyClass")
      .def(py::init<const py::array_t<double> &>())
      .def("getArray", &NumpyClass::getArray)
      .def("setArray", &NumpyClass::setArray);
}
